﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjetB3
{
    public class ResistanceInfos
    {
        public float totalRes = 0;
        public float shadowRes = 0;
        public float fireRes = 0;
        public float iceRes = 0;
        public float natureRes = 0;
        public float arcaneRes = 0;
    }
}
